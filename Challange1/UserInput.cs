﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challange1
{
   public class UserInput
    {
        int h1;
        int m1;
        int h2;
        int m2;
        public void Input()
        {
            try
            {
                //Get depatures time
                Console.WriteLine("Enter hour and minutes of departure time and minutes(01 05): ");
                string[] depatures = Console.ReadLine().Split();
                h1 = int.Parse(depatures[0]);
                m1 = int.Parse(depatures[1]);
            }
            catch(Exception)
            {
                Console.WriteLine("Please enter the valid time and minutes!");
                Input();
            }


            try
            {
                //get duration
                Console.WriteLine("Enter hour and minutes of duration (01 05): ");
                string[] duration = Console.ReadLine().Split();
                h2 = int.Parse(duration[0]);
                m2 = int.Parse(duration[1]);
            }
            catch(Exception)
            {
                Console.WriteLine("Please enter the valid time and minutes!");
                Input();
            }


            try
            {
                //calculate the arrival time
                int airm = m1 + m2;
                int airh = h1 + h2 + airm / 60;
                airm = airm % 60;
                airh = airh % 24;

                //display arrival time
                Console.WriteLine("Arival time is:" + airh + " " + airm);
                Console.ReadKey();
            }
            catch(Exception e)
            {
                Console.WriteLine("Entered time and mitues value format is incorrect!");
            }

          
        }
    }
}
